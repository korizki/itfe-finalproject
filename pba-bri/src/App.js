import Login from './component/login';
import Footer from './component/footer';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import PBAMMainPage from './pages/pbam-main';
import './App.css';
import './css/style.scss';
import './css/formmodal.scss';


function App() {
  return (
    <>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/pbammainpage" exact component={PBAMMainPage} />
      </Switch>
      <Footer />
    </BrowserRouter>
    </>
  );
}

export default App;
