import React, {useState, useEffect} from 'react';
import axios from 'axios';
import PBAMSidePanel from '../component/pbmsidepanel';

const PBAMDailyRep = () => {
        const [listcomment, setlistcomment] = useState(null)
        // get data from json api
        useEffect ( () => {
            if(listcomment === null) {
                axios.get('http://my-json-server.typicode.com/korizki/db/comment', {headers: {"Access-Control-Allow-Origin": "*"}} )
                .then( res => {
                    setlistcomment(res.data)
                    console.log(res.data)
                })
            } 
        }, [listcomment])
        return(
            <>
              <div className="flex">

                  {/* Komponen Sidepanel */}
                <PBAMSidePanel />
                <content className="mainpagebpba">
                    
                    {/* Page Title */}
                    <div className="pagetitle">
                        <h2>Daily Report Personal Asistant</h2>
                    </div>
                    <p>See Rating of Personal Asistant from this panel below</p>
                    
                    {/* Search Bar */}
                    <content className="searchBar">
                        <select id="category">
                            <option value="nama">Nama</option>
                            <option value="nama">Email</option>
                            <option value="nama">NIK</option>
                        </select>
                        <input type="text" className="inputkey inputreport" />

                        <span>Select Date</span>
                        <input className="inputdate" type="date" />
                        
                        <button><i className="fa fa-search fa-md" ></i>Search</button>
                    </content>

                    {/* Search Title Page */}
                    <div className="pagetitle">
                        <h2>Search Result </h2>
                    </div>

                    {/* Search Result Box */}
                    <div className="searchbar result boxreport">
                        <content className="mainbox report">

                            {/* Load / get data API */}
                            {
                                listcomment != null && listcomment.map((item, index) => 
                                    <div className="boxresult repdata">
                                        <div className="dataPA">
                                            <section className="iconpbarep">
                                            {/* Icon User Personal Asistant */}
                                                <img alt="profil" src={'https://www.pikpng.com/pngl/b/159-1591559_flat-faces-icons-circle-girl-flat-icon-png.png'} />
                                            </section>
                                            <section key={index}>
                                                <h3>{item.pbaname}</h3>
                                                <p><i className=" fa fa-calendar-day fa-lg"></i>Meeting Date : {item.meetingdate}</p>
                                                <p><i className="fa fa-clock fa-lg" ></i>Meeting Time : {item.meetingtime}</p>
                                            </section>
                                        </div>
                                        <div className="dataPCU">
                                            <h3>{item.pcuname}</h3>
                                            <p><i className="fa fa-phone-alt fa-lg"></i> {item.pcuphone}</p>
                                        </div>
                                        <div className="datarating">
                                            <h3>Rating :</h3>
                                            <p><i className=" fa fa-star fa-lg"></i>{item.ratingnumber}</p>
                                            <p title={item.comment}><a href="#"><i className=" fa fa-comment-dots fa-lg" ></i>Comment </a></p>
                                        </div>
                                    </div>    
                                )
                            }
                        </content>
                    </div>
                </content>
              </div>
            </>
        )
}

export default PBAMDailyRep;