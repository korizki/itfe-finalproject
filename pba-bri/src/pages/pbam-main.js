import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import TopNav from '../component/topnav';
import PBAMManagePA from './pbam-manage';
import PBAMDailyRep from './pbam-dailyreport';
import PBAMEditSch from './pbam-editsch';
// import Personlist from './tes';

const PBAMMainPage = () => {
	return(
		<>
		{/* Komponen Router Switch untuk direct link ke Page tertentu */}
		<BrowserRouter>

			{/* Atur Komponen Halaman yang tampil untuk User PBAM */}
			<Switch>
				<Route path="/pbammainpage" exact component={PBAMManagePA} />
				<Route path="/pbamdailyrep" exact component = {PBAMDailyRep} />
				<Route path="/pbameditsch" exact component = {PBAMEditSch} />
			</Switch>

			{/* Komponen Navigasi Atas */}
			<TopNav />
		</BrowserRouter>
		
		</>
	)
	
}

export default PBAMMainPage;