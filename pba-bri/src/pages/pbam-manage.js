import React, {useState, useEffect} from 'react';
import axios from 'axios';
import PBMSidePanel from '../component/pbmsidepanel';
import InputForm from '../component/modalform';

const PBAMManagePA = () => {
	// Fetch Data dari API Json
	const [listuser, setlistuser] = useState(null)
	useEffect( () => {
		if(listuser === null){
			axios.get('http://my-json-server.typicode.com/korizki/db/userPA',{headers: {"Access-Control-Allow-Origin": "*"}} )
			.then( res => {
				setlistuser(res.data)
				console.log(res.data)
			})
		}
	}, [listuser])

	// const submitSearch = (e) => {
	// 	e.preventDefault()
	// 	axios.get(`http://my-json-server.typicode.com/korizki/db/userPA`)
	// 	.then( res => {
	// 		let reslistuser = res.data.map(el => { return {
	// 			id: el.id,
				
	// 		}})

	// 	})
	// }

	return(
		<>
		{/* Flex element side nav dan main konten */}
		<div className="flex">

			{/* Element Side Panel */}
			<PBMSidePanel />

			{/* Element Input Form dalam bentuk Modal */}
			{/* <InputForm /> */}
			<div className="mainpagebpba">

				{/* Judul Halaman */}
				<div className="pagetitle">
					<div>
						<h2>Manage Personal Banking Asistant </h2>
					</div>
					<button><i className="fa fa-user-plus" ></i>Add Data</button>
				</div>
				<p>Find Personal Asistant from this panel below</p>

				{/* Bar Pencarian Data (Input dan button) */}
				<content className="searchBar">
					<select id="category">
						<option value="nama">Nama</option>
						<option value="nama">Email</option>
						<option value="nama">NIK</option>
					</select>
					<input className="inputkey" />
					<button><i className="fa fa-search fa-md" ></i>Search</button>
				</content>

				{/* Judul Konten Hasil Pencarian */}
				<div className="pagetitle">
					<h2>Search Result </h2>
				</div>

				{/* Elemen Bar untuk menampilkan data */}
				<content className="searchBar result">
					{
						listuser !== null && listuser.map((item, index) =>

						// Elemen Box Data 
						<div key={index} className="mainbox">
							<div className="boxresult">
								<section className="iconpba" key={index}>

									{/* Icon User Personal Asistant */}
									<img alt="profil" src={'https://www.pikpng.com/pngl/b/159-1591559_flat-faces-icons-circle-girl-flat-icon-png.png'} />
								</section>
								
								{/* Data Personal Asistant, seperti Nama, Rating, Email, dan No. Handphone */}	
								<section className="detailpba">
									<h3>{item.name}</h3>
									<p><i className="fa fa-stars fa-lg"></i><b>{item.rating}</b></p>
									<p><i className="fa fa-envelope fa-lg"></i>{item.email}</p>
									<p><i className="fa fa-phone-alt fa-lg"></i>{item.phone}</p>	
								</section>

								{/* Panel Button Action Edit dan Delete Data */}
								<section className="boxaction">
									<button className="edit"><i className="fa fa-edit"></i>Edit</button>
									<button className="delete"><i className="fa fa-trash-alt"></i>Delete</button>
								</section>	
							</div>
						</div>
						)
					}
				</content>
			</div>
		</div>
		</>
	)
}

export default PBAMManagePA;