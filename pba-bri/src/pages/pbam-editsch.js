import React, {useEffect, useState} from 'react';
import axios from 'axios';
import PBAMSidePanel from '../component/pbmsidepanel';


const PBAMEditSch = () => {
    const [listoffdate, setlistoffdate] = useState(null)
    useEffect(() => {
        if(listoffdate === null) {
            axios.get('http://my-json-server.typicode.com/korizki/db/offdate', {headers: {"Access-Control-Allow-Origin": "*"}})
            .then( res => {
                setlistoffdate(res.data);
                console.log(res.data);
            })
        }
    }, [listoffdate])
    return(
        <>
              <div className="flex">
                <PBAMSidePanel />
                <content className="mainpagebpba">
                    <div className="pagetitle">
                        <h2>Manage Schedule Personal Asistant</h2>
                    </div>
                    <p>Add date off / holidays of Personal Banking Asistant </p>
                    <content className="searchBar inputform">
                        <label className="inputtext date">
                            <p>Date</p>
                            <input type="date" />
                        </label>
                        <label className="inputtext ">
                            <p>Detail / Date Info</p>
                            <input spellCheck="false" type="text" />
                        </label>
                        <button className="submitholiday"><i className="fa fa-plus"></i>Add</button>
                    </content>
                    <div className="pagetitle">
                        <h2>List of Holiday / Off Schedule </h2>
                    </div>
                    <div className="boxreport result listholiday">
                        <table>
                            <tbody>
                                <tr>
                                    <th>Date</th>
                                    <th>Detail / Day Info</th>
                                    <th>Action</th>
                                </tr>
                                {
                                    listoffdate !== null && listoffdate.map((item, index) => 
                                        <tr key={index}>
                                            <td>{item.date}</td>
                                            <td><span style={{fontWeight: "600"}}>{item.info}</span></td>
                                            <td className="buttonsch">
                                                <button className="edit"><i className="fa fa-edit"></i>Edit</button>
                                                <button className="delete"><i className="fa fa-trash-alt"></i>Delete</button>
                                            </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </content>
              </div>
            </>
    )
}

export default PBAMEditSch;