import React from 'react';

class Footer extends React.Component{
    render(){
        return(
            <div>
                <footer>
                    &copy; 2020, PT. Bank Rakyat Indonesia (Persero) Tbk. - Priority Banking Asistant System App. 
                </footer>
            </div>
        )
    }
}

export default Footer;