import React from 'react';
import loginIcon from '../assets/icon/loginIcon.png';
import briIcon from '../assets/icon/bri-c.png';
import dtsIcon from '../assets/icon/dts.png';
import {Link} from 'react-router-dom';
import InputForm from './modalform';

class Login extends React.Component{
    
    componentDidMount(){
        //set kursor ke input Username saat halaman dimuat pertama kali
        this.nameInput.focus();
    }
    render(){   
        return(
            <>
            <InputForm />
            <div className="topLogin">

                {/* Top Logo DTS dan BRI */}
                <section className="headIcon">
                    <img  className="briIcon" alt="logoBRI" src={briIcon} />
                    <img alt="logoDTS" src={dtsIcon} />
                </section>
                
                {/* Konten Halaman Login, Gambar dan Form Login */}
                <content className="loginPage">    

                    {/* Gambar Ilustrator Login */}
                    <figure className="loginIcon">
                        <img title="Designed by pikisuperstar / Freepik" alt="logoLogin" src={loginIcon} />
                    </figure>

                    {/* Login Form Komponen (Input dan Button) */}
                    <section className="loginForm">
                        <h3> Log In Form</h3>

                        {/* Komponen Input Username */}
                        <label className="labelInput">
                            <i className="fa fa-user"></i>
                            <input spellCheck="false" ref= {(input) => {this.nameInput = input }} className="inputField" placeholder= "Username" type="text" required id="userinput" name="userField" />
                        </label>

                        {/* Komponen Input Password */}
                        <label className="labelInput">
                            <i className="fa fa-key"></i>
                            <input className="inputField" placeholder= "Password" type="password" required name="passField" />
                        </label>

                        {/* Komponen Button Login, menggunakan Komponen Link untuk direct ke Halaman Homepage */}
                        <Link to="/pbammainpage"><button className="loginSubmit">Log In</button></Link>
                        <p>Forgot Password? <a href="https://www.google.com">Click here!</a> </p>
                    </section>
                </content>

                {/* Info pengguna Web */}
                <content className="info">
                    <p>Please contact your <b>Administrator</b>, if you don't have an <b> Account</b>, or getting problem when <b>Log In</b>.</p>
                </content>
            </div>
            </>
        )
    }
}

export default Login;