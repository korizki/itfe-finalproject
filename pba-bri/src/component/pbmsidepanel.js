import React from 'react';
import {Link} from 'react-router-dom';

class PBMSidePanel extends React.Component{
    render(){
        return(
            <div className="pbmsidepanel">
                <content>
                    {/* Judul Komponen Side Nav */}
                    <p style={{marginBottom: "20px", fontWeight: "600"}}>Side Menu</p>
                    
                    {/* Link pertama ke Halaman Daily Report */}
                    <div>
                        <Link to="/pbamdailyrep"> <i className="fa fa-file-check fa-lg"></i>
                        Daily Report </Link>
                    </div>
                    {/* Link kedua ke Halaman Manage PA */}
                    <div>
                        <Link to ="/pbammainpage"><i className="fa fa-users-class fa-lg"></i>
                        Manage Personal Asistant</Link>
                    </div>
                    {/* Link ketiga ke Halaman Edit Schedule */}
                    <div>
                        <Link to ="/pbameditsch"><i className="fa fa-calendar-check fa-lg"></i>
                        Set Off Schedule</Link>
                    </div>
                </content>
            </div>
        )
    }
}

export default PBMSidePanel;