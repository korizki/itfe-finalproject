import React from 'react';
import {Link} from 'react-router-dom';

class TopNav extends React.Component{
    render(){
        return(
            // Komponen Navigasi Atas
            <div className="topNav">
                
                {/* Icon user, welcome word */}
                <p> <img alt="admin icon" className="naviconuser" src={'https://img.icons8.com/bubbles/2x/admin-settings-male.png'} />Welcome back, <span id="user">Rizki Ramadhan</span> </p>

                {/* Info user role yang login */}
                <p><i className="fa fa-user-tie fa-lg"></i>You're logged in as <span id="role">Personal Asistant Manager</span></p>

                {/* Link untuk Log Out User */}
                <Link to ="/"><p onClick='window.location.reload()'  className="logout"><i className="fa fa-sign-out fa-lg"></i>Log Out</p></Link>
            </div>
        )
    }
}

export default TopNav;