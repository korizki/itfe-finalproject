import React from 'react';

const InputForm = () => {
    
        return(
            <>
            <div className="modal">
                <div className="modalcontent">
                    <span className="close">&times;</span>
                    <form>
                        <h2>Add Data User</h2>
                        <content className="formadddata">
                            <label>
                                <input type="text" required placeholder="."/>
                                <p>Full Name</p>
                            </label>
                            <label>
                                <input type="text" required placeholder="."/>
                                <p>Identity Number</p>
                            </label>
                            <label>
                                <input type="text" required placeholder="."/>
                                <p>Phone Number</p>
                            </label>
                            <label>
                                <input type="text" required placeholder="."/>
                                <p>Address</p>
                            </label>
                            <label>
                                <input type="email" required placeholder="."/>
                                <p>Email</p>
                            </label>
                            <label>
                                <input type="text" required placeholder="."/>
                                <p>Username</p>
                            </label>
                            <label>
                                <input type="password" required placeholder="."/>
                                <p>Password</p>
                            </label>
                            <input type="submit" className="btnsubmit" value="Save" />
                        </content>
                    </form>
                </div>
            </div>
            </>
        )
}

export default InputForm;